import React, {Component} from 'react';
import Prefeituras from './Prefeituras/Prefeituras';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
        </div>
        <p className="App-intro">
          Acompanhe aqui o status das Prefeituras entregues até o momento.
        </p>
        <Prefeituras />
      </div>
    );
  }
}

export default App;
