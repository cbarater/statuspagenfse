import React, {Component} from 'react';
import listaMunicipio from '../lista-municipio.json';

function initial (value) {
  return value || [];
}

const lista = listaMunicipio.reduce((acc, item) => {
  return {
    ...acc,
    [item.uf]: [...initial(acc[item.uf]), item]
  }
}, {})

export default class ListaPrefeituras extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: null,
    };
  }
  
  changeOpen(uf) {
    if (this.state.open === uf) {
      return this.setState({ open: null });
    }
    this.setState({ open: uf })
  }

  renderCidades(item, uf) {
    const className = (uf === this.state.open) ? ' is-active' : '';       
    return (
      <ul className={'Cidades' + className}>
        {item.map((municipio)=> (
          <li key={municipio.municipio}>{municipio.municipio}</li>
        ))}
      </ul>
    );
  }

  render () {
    return (
      <div className="Lista-prefeitura">
        <ul className="Estados">
          {Object.keys(lista).sort().map((key) => {
            const item = lista[key];
            return (
              <li key={key}>
                <div className="Nome_Estado" onClick={() => this.changeOpen(key)}>{key}</div>
                {this.renderCidades(item, key)}
              </li>
             ); 
          })}
        </ul>
      </div>
    );
  }
}