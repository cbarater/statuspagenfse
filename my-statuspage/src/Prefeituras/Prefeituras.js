import React, {Component} from 'react';
import ListaPrefeituras from './ListaPrefeituras';


export default class Prefeituras extends Component {
  render() {
    return (
      <div className="Prefeituras">
        <ListaPrefeituras />
      </div>
    );
  }
}